SHELL := /bin/bash
# config for dia
DIA_C = dia
DIA_SRCS=$(shell find -type f -iname "*.dia")
DIA_OBJS=$(DIA_SRCS:.dia=.png)
# config for libre office documents
ODG_C = libreoffice
ODG_SRCS=$(shell find -type f -iname "*.odg")
ODG_OBJS=$(ODG_SRCS:.odg=.pdf)
# config for markdown
MD_C = pandoc
MD_SRCS=$(shell find -type f -iname "*.md")
MD_OBJS=$(MD_SRCS:.md=.pdf)

# NOTICE:
# $@ left site of :
# $^ right site of :
# $< right site but only the current element
# the file to get: the dependencies
# %.md a file.md

# build them all
all: dia odg md seminararbeit

#######
# DIA #
#######
%.png: %.dia
	$(DIA_C) $< -e $@ $(OUTPUT)

dia: $(DIA_OBJS)

#######
# ODG #
#######
%.pdf: %.odg
	$(ODG_C) --convert-to pdf --outdir `dirname $<` $< $(OUTPUT)

odg: $(ODG_OBJS)

######
# MD #
######
%.pdf: %.md
	$(MD_C) --from=markdown --to=pdf -t latex $< -o $@

md: $(MD_OBJS)

#################
# SEMINARARBEIT #
#################
seminararbeit:
	make -C abgaben/ausarbeitung/

#########
# CLEAN #
#########
clean:
	rm -f $(ODG_OBJS) $(DIA_OBJS) $(MD_OBJS) $(SEMINARARBEIT_OBJS)
	make -C abgaben/ausarbeitung/ clean

clean_all:
	rm -f $(ODG_OBJS) $(DIA_OBJS) $(MD_OBJS) $(SEMINARARBEIT_OBJS) log.txt error_log.txt
	make -C abgaben/ausarbeitung/ clean_all

