# Lizenz
- Diese Arbeit ist unter [cc by-sa 3.0 de](https://creativecommons.org/licenses/by-sa/3.0/de/) lizensiert
- Ausgenommen von der Lizenz sind folgende Dateien:
    - alles in [documents](documents) - Keine Lizenzangabe
    - [abgaben/ausarbeitung/common/fh-layout.tex](fh-layout.tex) - Keine Lizenzangabe
    - [abgaben/ausarbeitung/img/FHAAC_4C_BL-r.pdf](FHAAC_4C_BL-r.pdf) - [Keine Lizenzangabe](https://docplayer.org/5408519-Corporate-design-fh-aachen-university-of-applied-sciences.html)
    - [abgaben/praesentation/img/monolith1.jpg](monolith1.jpg) - Lizenz in [abgaben/praesentation/img/monolith1.txt](monolith1.txt)
