# Seminararbeit
Dies ist mein Repository für die Seminararbeit WS18.

## Vergleich von Modularisierungskonzepten bei der Entwicklung von Webschnittstellen
In meiner Seminararbeit werde ich verschiedene Möglichkeiten vergleichen, wie man eine große Codebasis in kleinere, übersichtliche Module aufteilt. Dabei werde ich mir verschiedene Plugin- sowie Microservicearchitekturen anschauen.

## Schriftliche Abgaben
- Kurzfassung, ca. eine A4-Seite
- Schriftliche Ausarbeitung, ca. 25-30 Seiten
- Folien für den Vortrag

## Ablauf
- Laufzettel ausfüllen und Seminararbeit in den Matse-Diensten eintragen, bis 15. Okt
- Anfertigung der schriftlichen Ausarbeitung, bis 15. Dez, 2 Monate Arbeit
	- Währenddessen am besten direkt die Zusammenfassung schreiben.
	- Dokumentation auf dem Laufzettel
- Abgabe der Präsentation, Januar
- Am Ende, Vortrag vor Auditorium

## Schriftliche Ausarbeitung
- Muss eine eidesstattliche Erklärung beinhalten
- Pro Woche 20h arbeiten + 1 Kapitel schreiben
- Insgesammt 2 Monate Zeit
- Den genauen Umfang noch mit Frau Merkel besprechen
- Insgesammt 25-30 Seiten schreiben
- Mitte Dezember ist Abgabe

### Aufbau
1. Einleitung (_2-3 Seiten_)
2. Was ist Modularisierung? (_5 Seiten_)
	- Welche Architekturen gibt es
		- Modul, Komponenten, Modularisierung
	- Beispiel der Modularisierung wie es ausschauen könnte bei unserer API
	- Welche Kategorien schaue ich mir an:
		- Monolythisch
		- Plugins
		- Microservices
3. Kriterien/Anforderungen (_2-3 Seiten_)
	- Geschwindigkeit / größe des Frameworks / wie komplex ist das / Ziel der Arbeit
4. Vorstellung der Architekturen (_10-15 Seiten_)
	1. Monolithische Architekturen
	2. Plugin-Architekturen
	3. Microservices-Architekturen
	- In den Unterpunkten jeweils auch verschiedene konkretere Architekturen der jeweiligen Kategorie beschreiben.
5. Vergleich der Architekturen anhand der Kriterien (_mit Fazit 5 Seiten_)
6. Fazit

### Zeitplanung
- 2 Wochen - Einleitung, Was ist Modularisierung und Kriterien/Anforderungen
- 4 Wochen - Vorstellung der Architekturen (bis 28.11)
- 1 Woche - Vergleich und Fazit
- 1 Woche - Puffer

## Tools
### Dia 0.97.3, LibreOffice
- creating diagrams

### Converting README.md to PDF
- `pandoc`
	- convert markdown to html

### Compile LaTeX
- `latexmk` + `xelatex`
	- compile seminararbeit.tex to pdf
	- `latexmk -xelatex -pvc seminararbeit.tex`
